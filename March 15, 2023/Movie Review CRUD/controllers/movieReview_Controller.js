const Review = require('../models/MovieReview_models');

// CREATE MovieReview...................................................
module.exports.addMovieReview = (reqBody) => {
    let newReview = new Review({
        title: reqBody.title,
        yearReleased: reqBody.yearReleased,
        rating: reqBody.rating,
        review: reqBody.review,
    });

    return newReview
        .save()
        .then((review) => true)
        .catch((err) => false);
};

// RETRIEVE ALL MovieReview ..............................................................
module.exports.getAllReviews = () => {
    return Review.find({})
        .then((result) => result)
        .catch((err) => err);
};

// UPDATE Movie Reviews .........................................

module.exports.updateReview = (reqParams, reqBody) => {
    let updatedProduct = {
        title: reqBody.title,
        yearReleased: reqBody.yearReleased,
        rating: reqBody.rating,
        review: reqBody.review,
    };

    return Review.findByIdAndUpdate(reqParams.movieReviewId, updatedProduct)
        .then((product) => {
            return true;
        })
        .catch((err) => err);
};
