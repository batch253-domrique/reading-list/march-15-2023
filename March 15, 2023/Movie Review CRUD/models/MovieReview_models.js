const mongoose = require('mongoose');

const movieReviewSchema = new mongoose.Schema({
    title: {
        type: String,
        required: [true, 'Title is required'],
    },
    yearReleased: {
        type: String,
        required: [true, 'Year Released is required'],
    },
    rating: {
        type: Number,
        required: [true, 'Rating is required'],
    },
    review: {
        type: String,
        required: [true, 'Review is required'],
    },
    movieReview: [
        {
            movieId: {
                type: String,
                required: [true, 'movieId is required'],
            },
        },
    ],
});

module.exports = mongoose.model('Review', movieReviewSchema);
