const express = require('express');
const router = express.Router();
const movieReviewController = require('../controllers/movieReview_Controller');

//ROUTES : CREATE MovieReview .....................................
router.post('/', (req, res) => {
    movieReviewController
        .addMovieReview(req.body)
        .then((resultFromController) => res.send(resultFromController))
        .catch((err) => res.send(err));
});

// ROUTES: RETRIEVE ALL MovieReview. ...........................................
router.get('/all', (req, res) => {
    movieReviewController
        .getAllReviews()
        .then((resultFromController) => res.send(resultFromController))
        .catch((err) => res.send(err));
});

// ROUTE: UPDATE Movie Review .........................................
router.put('/:reviewId', (req, res) => {
    movieReviewController
        .updateReview(req.params, req.body)
        .then((resultFromController) => res.send(resultFromController))
        .catch((err) => res.send(err));
});
//Do not touch by touch.......................................
module.exports = router;
