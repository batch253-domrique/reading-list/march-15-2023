//directives to load Node.Js/////////////////////////////////////////////////////
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const movieReviewRoute = require('./routes/movieReview_Routes');
const app = express();
const port = process.env.PORT || 4000;
const db = mongoose.connection;

// Connect to MOngoDB database////////////////////////////////////////////////
mongoose.connect(
    'mongodb+srv://admin:admin123@batch253-domrique.wk1bqpw.mongodb.net/ReadingList-wedensday?retryWrites=true&w=majority',
    {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    }
);
db.once('open', () => console.log('Now connected to MongoDB Atlas.'));

//app.use///////////////////////////////////////////////////////////////////////
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//routes/////////////////////////////////////////////////////////////////////////

app.use('/reviews', movieReviewRoute);

//This is to listen//////////////////////////////////////////////////////////////
if (require.main === module) {
    app.listen(port, () => {
        console.log(`API is now online on port ${port}`);
    });
}
///////////////////////////////////////////////////////////////////////////////////

module.exports = app;
